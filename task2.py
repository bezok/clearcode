"""
Module to calculate damage from spell
"""


def damage(spell):
    """
    Function calculating damage
    :param str spell: string with spell
    :rtype: int
    :return points of damage
    """

    fe_damage = 1                      # if subspells values can change

    current_damage = fe_damage

    to_delete_index = 0


    spell = is_spell_correct(spell)

    if not spell:
        return 0


    while True:
        spell = spell[to_delete_index:len(spell)]


        if not spell:
            if current_damage > 0:
                return current_damage

            return 0


        next_subspells = get_next_subspells(spell)

        new_damage, to_delete_index = calculate_damage(next_subspells)
        current_damage += new_damage



def calculate_damage(next_subspells):
    """
    Function calculating damage from subspells
    :param dict next_subspells
    :rtype tuple (int, int)
    :return: damage from subspells and value to delete
    """

    subspells = {"je": 2, "ne": 2, "ai": 2, "jee": 3, "ain": 3, "dai": 5}

    new_damage = 0
    key_3_damage = 0
    key_2_damage = 0
    key_len = 0
    je_flag = False
    jee_flag = False


    if not next_subspells:
        return (-1, 1)


    for key in next_subspells:
        if len(key) == 3:
            key_3_damage = subspells[key]

            if key == "jee":
                jee_flag = True
        else:
            key_2_damage += subspells[key]
            key_len += 2

            if key == "je":
                je_flag = True


    if jee_flag and je_flag:
        return (5, 5)           # "je" + "jee" = 2 + 3, same for length
                                # if subspells values can change then replace (5, 5)
                                # with (subspells["je"]+subspells["jee"], 5)

    if key_3_damage > key_2_damage:
        new_damage += key_3_damage
        to_delete_index = 3
    else:
        new_damage += key_2_damage
        to_delete_index = key_len


    return (new_damage, to_delete_index)



def is_spell_correct(spell):
    """
    Function that check if spell is corrent ("fe" on start and "ai" on end)
    :param str spell: string with spell
    :rtype string
    :return: spell without: -all before "fe"
                            -"fe"
                            -all after last "ai"
    empty when spell is incorrect
    """

    fe_index = spell.find("fe")
    ai_index = spell.rfind("ai")

    if fe_index is not -1 and ai_index is not -1:
        spell = spell[fe_index+2:ai_index+2]
        # if "dai" cannot end spell then  ^ remove 2 from here and add 2 damage
        # to current_damage in damage function
        # but without "d" it will makes less damage (example "fe dai": 1 + 5, "fe d ai": 1 - 1 + 2)
        # --> "Spell start with 'fe' and end with 'ai'"

        if spell.find("fe") is not -1:
            spell = ""
    else:
        spell = ""

    return spell



def get_next_subspells(spell):
    """
    Function that getting next subspells from spell
    :param str spell: string with spell
    :rtype: dict
    :return: dict with next subspells
    """

    subspells = {"je": 2, "ne": 2, "ai": 2, "jee": 3, "ain": 3, "dai": 5}
    next_subspells = {}
    on_zero = False

    for key in subspells:
        i = spell.find(key)
        if i is not -1:
            if i < 3:
                next_subspells[key] = i
                if i is 0:
                    on_zero = True

    if not on_zero:
        next_subspells = {}

    return next_subspells
