""" Module with "rocket science" script """



def group_by(stream, field, succes=None):
    """
    Function aggregating satellite launches count by "field"
    :param stream: stream with launches
    :param field: string with "month" or "year"
    :param succes: filter if launch succeeded (None - without filter)
    :rtype: dict
    :return: dict of aggregations {FIELD: int}
    """

    fields = {}

    if field == 'month':
        column = 18                         # position in line
        column_end = 21
    else:
        column = 13
        column_end = 17

    column_succes = 193

    current_field = 0
    succes_flag = False


    for line in stream:
        if line[0].isdigit():
            if succes is True and line[column_succes] == "F":
                succes_flag = False
                continue
            elif succes is False and line[column_succes] == "S":
                succes_flag = False
                continue

            succes_flag = True
            current_field = line[column:column_end]

        else:
            if not succes_flag:
                continue


        if current_field in fields:
            fields[current_field] += 1
        else:
            fields[current_field] = 1



    return fields
